from flask import Flask , jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS,cross_origin
from main.configure.config import Config
app = Flask("EmployeeAPP")
CORS(app)

app.config.from_object(Config)

db = SQLAlchemy(app)

from main.blueprints.users.routes import users
from main.blueprints.hr.routes import HRROUTE

app.register_blueprint(users , url_prefix ="/user")
app.register_blueprint(HRROUTE , url_prefix ="/hr")

@app.route("/hello")
def hello():
    return jsonify({"message":"Hello back"} , 200)