import os
import json
from flask import current_app , app

# Get data from enviroment
host =os.environ['DB_HOST'] 
password =os.environ['DB_PASSWORD'] 
db =os.environ['DB_NAME'] 
user =os.environ['DB_USER'] 
SQL_PATH =f"mysql://{user}:{password}@{host}/{db}" 

try:
    with open("/etc/sharely_conf.json")as conf_file:
        configure=json.load(conf_file)
except:
    with open("main/configure/app_configure.json") as conf_file:
        configure=json.load(conf_file)

class Config:
    
    SECRET_KEY = configure.get("secret_key")  
    SQLALCHEMY_DATABASE_URI = SQL_PATH
