from main import db , app

class Hr(db.Model):
    id = db.Column(db.Integer , primary_key = True) 
    username = db.Column(db.Text(80))
    password = db.Column(db.Text(80), unique=False)

class Employees(db.Model):
    id = db.Column(db.Integer , primary_key=True) 
    emp_name = db.Column(db.Text(80)) 
    emp_salery = db.Column(db.Integer) 
    hr_id = db.Column(db.ForeignKey('hr.id')) 
    deleted = db.Column(db.Boolean , default = False)



