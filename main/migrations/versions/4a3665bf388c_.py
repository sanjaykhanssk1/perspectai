"""empty message

Revision ID: 4a3665bf388c
Revises: 
Create Date: 2020-12-05 13:57:31.238739

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4a3665bf388c'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('employess',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.Text(), nullable=True),
    sa.Column('image_url', sa.Text(), nullable=True),
    sa.Column('dob', sa.DateTime(), nullable=True),
    sa.Column('email', sa.Text(), nullable=True),
    sa.Column('password', sa.Text(), nullable=True),
    sa.Column('description', sa.Text(), nullable=True),
    sa.Column('datetime', sa.DateTime(), nullable=True),
    sa.Column('deleted', sa.Boolean(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('employee_address',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('emp_id', sa.Integer(), nullable=True),
    sa.Column('address', sa.Text(), nullable=True),
    sa.Column('code', sa.Text(), nullable=True),
    sa.Column('contact', sa.Text(), nullable=True),
    sa.Column('pan', sa.Text(), nullable=True),
    sa.Column('aadhaar', sa.Text(), nullable=True),
    sa.ForeignKeyConstraint(['emp_id'], ['employess.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('employee_worked',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('emp_id', sa.Integer(), nullable=True),
    sa.Column('date', sa.DateTime(), nullable=True),
    sa.Column('day_starts', sa.DateTime(), nullable=True),
    sa.Column('day_ends', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['emp_id'], ['employess.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('employee_worked')
    op.drop_table('employee_address')
    op.drop_table('employess')
    # ### end Alembic commands ###
