"""empty message

Revision ID: 0f814514cb54
Revises: 1074ae915a0e
Create Date: 2020-12-05 14:53:15.972505

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '0f814514cb54'
down_revision = '1074ae915a0e'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('HR',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('username', sa.Text(length=80), nullable=True),
    sa.Column('password', sa.Text(length=80), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('employees',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('emp_name', sa.Text(length=80), nullable=True),
    sa.Column('emp_salary', sa.Integer(), nullable=True),
    sa.Column('deleted', sa.Boolean(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.drop_table('employess')
    op.drop_table('employee_worked')
    op.drop_table('employee_address')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('employee_address',
    sa.Column('id', mysql.INTEGER(display_width=11), autoincrement=True, nullable=False),
    sa.Column('emp_id', mysql.INTEGER(display_width=11), autoincrement=False, nullable=True),
    sa.Column('address', mysql.TEXT(), nullable=True),
    sa.Column('code', mysql.TEXT(), nullable=True),
    sa.Column('contact', mysql.TEXT(), nullable=True),
    sa.Column('pan', mysql.TEXT(), nullable=True),
    sa.Column('aadhaar', mysql.TEXT(), nullable=True),
    sa.ForeignKeyConstraint(['emp_id'], ['employess.id'], name='employee_address_ibfk_1'),
    sa.PrimaryKeyConstraint('id'),
    mysql_default_charset='utf8mb4',
    mysql_engine='InnoDB'
    )
    op.create_table('employee_worked',
    sa.Column('id', mysql.INTEGER(display_width=11), autoincrement=True, nullable=False),
    sa.Column('emp_id', mysql.INTEGER(display_width=11), autoincrement=False, nullable=True),
    sa.Column('date', mysql.DATETIME(), nullable=True),
    sa.Column('day_starts', mysql.DATETIME(), nullable=True),
    sa.Column('day_ends', mysql.DATETIME(), nullable=True),
    sa.ForeignKeyConstraint(['emp_id'], ['employess.id'], name='employee_worked_ibfk_1'),
    sa.PrimaryKeyConstraint('id'),
    mysql_default_charset='utf8mb4',
    mysql_engine='InnoDB'
    )
    op.create_table('employess',
    sa.Column('id', mysql.INTEGER(display_width=11), autoincrement=True, nullable=False),
    sa.Column('name', mysql.TEXT(), nullable=True),
    sa.Column('image_url', mysql.TEXT(), nullable=True),
    sa.Column('dob', mysql.DATETIME(), nullable=True),
    sa.Column('email', mysql.TEXT(), nullable=True),
    sa.Column('password', mysql.TEXT(), nullable=True),
    sa.Column('description', mysql.TEXT(), nullable=True),
    sa.Column('datetime', mysql.DATETIME(), nullable=True),
    sa.Column('deleted', mysql.TINYINT(display_width=1), autoincrement=False, nullable=True),
    sa.PrimaryKeyConstraint('id'),
    mysql_default_charset='utf8mb4',
    mysql_engine='InnoDB'
    )
    op.drop_table('employees')
    op.drop_table('HR')
    # ### end Alembic commands ###
