from flask import jsonify, abort , make_response
class RestHandler():

    def __init__(self , class_to_call, method, request, current_user = None):
        
        cls_ = class_to_call(request , current_user)
        code = 200
        response = {"message" : "Hello"}
        if method.upper() == "GET":
            response , code  = cls_.GET()
        
        if method.upper() == "POST":
            response , code = cls_.POST()
        
        if method.upper() == "PUT":
            response ,code = cls_.PUT()
        
        if method.upper() == "DELETE":
            response,code  = cls_.DELETE()
        
        # Add aditional detail to response if needs
        abort(make_response(response, code))