from main.models.employees import Employees
from datetime import datetime
from flask import abort
from main import app , db

class Account():
    """Class handles all about employee - GET, POST, EDIT , DELETE"""
    def __init__(self , request , current_user):
        self.request = request 
        self.current_user =  current_user
        self.data = self.request.get_json()

    def GET(self):
        emp_id =self.request.args.get("emp_id")
        emp = Employees.query.filter_by(id = emp_id).first()

        if emp is not None:
            return {"name":emp.emp_name , "salary":emp.emp_salery} , 200
        else:
            return {"message":"No user found"} , 404

    def POST(self):

        new = Employees()
        new.emp_name = self.data["name"]
        new.emp_salery = self.data["salery"]

        try:
            db.session.add(new)
            db.session.commit()
        except KeyError as e:
            return {"message": f"{e} is missing"},402
        except Exception as e:
            return {"message": "Server Error"},500
        return {"message":"Account created" , "employee-id":new.id} , 200

    def PUT(self):

        emp = Employees.query.filter_by(id = self.data["id"]).first()
        emp.emp_name =self.data["name"] 
        emp.emp_salery = self.data["salery"]
        try:
            db.session.commit()
        except KeyError as e:
            return {"message": f"{e} is missing"},402
        except Exception as e:
            return {"message": "Server Error"},500
        return {"message":"Account Edited"} , 200

    def DELETE(self):
        emp = Employees.query.filter_by(id = self.data["id"]).first()
        emp.deleted = True
        db.session.commit()
        return {"message":"Account Deleted"} , 200

