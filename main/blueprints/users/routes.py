from flask import Blueprint, request
from main.blueprints.restHandler import RestHandler
from main.blueprints.users.classes.users import Account
from main import app
from main.models.employees import Hr, Employees
from flask import jsonify
import jwt
from functools import wraps
users = Blueprint( "users", __name__)


"""Authentication for Updating, deleting employee records"""
def hr_required(f):
    # token = request.headers("")
    @wraps(f)
    def decorated(*args , **kwargs):
        token = None
        if "x-access-token" not in request.headers:
            return jsonify({'error':1,"message":"Token is missing" , "code":401})

        token = request.headers["x-access-token"]
        if not token:
            return jsonify({"message":"token missing"})
        try:
            HR = jwt.decode(token , app.config["SECRET_KEY"])
            hr = Hr.query.filter_by(id=HR["HRid"]).first()

        except Exception as e:
            print(e)
            return jsonify({'error':1,"message":"Token is invalid" , "code":401})

        return f(hr , *args , **kwargs)

    return decorated

# @hr_required
@users.route("register" , methods=["POST"])
def registerEmployee():
    # data = request.get_json()
    return RestHandler(Account, "POST" , request )

@users.route("list_employees" , methods=["GET"])
def listEmployees():
    emps = Employees.query.filter_by(deleted = False).all()
    result = []
    for emp in emps:
        temp = {}
        temp["name"] = emp.emp_name
        temp["salery"]=emp.emp_salery
        temp["Hr"] = emp.hr_id
        result.append(temp)
    return {"employess":result}

@users.route("get_employee" , methods=["GET"])
@hr_required

def getEmployee(hr):
    RestHandler(Account, "GET" , request)


@users.route("edit_employee" , methods=["PUT"])
@hr_required
def editEmployee(hr):
    RestHandler(Account, "PUT" , request)

@users.route("delete_employee" , methods=["PUT"])
@hr_required
def deleteEmployee(hr):
    RestHandler(Account, "DELETE" , request)