from flask import Blueprint, request
from main.blueprints.restHandler import RestHandler
from .classes.hr import HR,HrAuth

HRROUTE = Blueprint( "hrs", __name__)

@HRROUTE.route("add_hr" , methods=["POST"])
def addHR():
    # data = request.get_json()
    return RestHandler(HR, "POST" , request )

@HRROUTE.route("login" , methods=["POST"])
def loginHR():
    # data = request.get_json()
    return RestHandler(HrAuth, "POST" , request )