from main import db,app
from main.models.employees import Hr, Employees
from passlib.hash import sha256_crypt
from flask import abort , make_response
import jwt

"""Class handles all about HR - Create HR account"""

class HR():
    def __init__(self, request , current_user):
        self.request = request
        self.current_user = current_user
        self.data = self.request.get_json()
    
    @staticmethod
    def checkUsenameAvaialbe(username):
        isExists = Hr.query.filter_by(username=username).first()
        if isExists:
            abort(make_response({"message":"Username not avialable"},404))

    def POST(self):
        
        
        emp = Employees.query.filter_by(id = self.data["emp_id"]).first()
        if emp is None:
            abort(make_response({"message":"No employee found"},404))
        if emp.hr_id is not None:
            abort(make_response({"message":"Already HR"},404))
        self.checkUsenameAvaialbe(self.data["username"])


        new = Hr()
        new.username = self.data["username"]
        new.password = sha256_crypt.encrypt(self.data["password"])
        try:
            db.session.add(new)
            db.session.commit()
            emp.hr_id = new.id
            db.session.commit()
            return {"message":"Added"} , 200
        except Exception as e:
            print(e)
            return {"message":"Internal error"} , 500
    def GET(self):
        # Hr.query.filter_by(usernmae = self.)
        pass
    def PUT(self):
        pass

    def DELETE(self):
        pass

"""Class handles all authentication about HR - Login """

class HrAuth():
    def __init__(self, request , current_user):
        self.request = request
        self.current_user = current_user
        self.data = self.request.get_json()

    def POST(self):
        hr = Hr.query.filter_by(username = self.data["username"]).first()
        if hr is None:
            abort("No Hr found", 404)
        if not sha256_crypt.verify(self.data["password"] , hr.password) :
            abort("password mismatch" , 401)

        token = self.generate_token(hr.id)
        return {"message":"Login success" , "auth-token":token},200
    
    @staticmethod
    def generate_token(HRid):
        token = jwt.encode({"HRid":HRid},
                            app.config["SECRET_KEY"])
        return token.decode("utf-8")


    def GET(self):
        # Hr.query.filter_by(usernmae = self.)
        pass
    def PUT(self):
        pass

    def DELETE(self):
        pass